/* eslint-disable snakecasejs/snakecasejs */
module.exports = {
	root: true,
	parser: "@typescript-eslint/parser",
	plugins: ["import", "snakecasejs"],
	extends: [
		"plugin:react/recommended", // Common rules commonly regarded as a good practice
		"airbnb-typescript",
		"plugin:import/warnings",
		"plugin:import/errors",
		"plugin:import/typescript",
		"plugin:@typescript-eslint/eslint-recommended", // Disable rules already covered by the TypeScript compiler
		"plugin:@typescript-eslint/recommended", // TypeScript rules commonly regarded as a good practice
	],
	parserOptions: {
		ecmaVersion: 2020,
		sourceType: "module", // Allows for the use of imports
		ecmaFeatures: { jsx: true },
		project: "./tsconfig.eslint.json",
	},
	rules: {
		/**************/
		/* Formatting */
		/**************/
		"react/self-closing-comp": "error",

		indent: ["error", "tab"],
		"no-tabs": ["error", { allowIndentationTabs: true }],
		"@typescript-eslint/indent": ["error", "tab", { SwitchCase: 1 }],
		"react/jsx-indent": ["error", "tab"],
		"react/jsx-indent-props": ["error", "tab"],

		"@typescript-eslint/semi": [
			"error",
			"never",
			{
				beforeStatementContinuationChars: "always",
			},
		],
		"object-curly-newline": ["error", { multiline: true, consistent: true }],
		"@typescript-eslint/quotes": ["error", "double"],
		"spaced-comment": [
			"error",
			"always",
			{
				markers: [
					// Allow TypeScript `/// <reference ...>` and similar comments
					"/",
				],
				exceptions: [
					// Allow 'box-style' (`/***/`) comments
					"***",
				],
			},
		],
		"max-len": [
			"error",
			{ code: 80, tabWidth: 1, ignoreTrailingComments: true },
		],
		"array-element-newline": ["error", "consistent"],
		"array-bracket-newline": ["error", "consistent"],
		"no-multi-spaces": [
			"error",
			{
				ignoreEOLComments: true,
				exceptions: { Property: false },
			},
		],
		"@typescript-eslint/member-delimiter-style": [
			"error",
			{
				multiline: {
					delimiter: "none",
				},
			},
		],
		"@typescript-eslint/camelcase": "off",
		/********/
		/* Misc */
		/********/
		"@typescript-eslint/explicit-function-return-type": "off",
		"import/prefer-default-export": "off",
		"no-nested-ternary": "off",
	},
	overrides: [
		{
			files: ["**/*.tsx"],
			rules: { "react/prop-types": "off" },
		},
	],
	settings: {
		react: { version: "detect" },
	},
}
