import React from "react"

export const Basic: React.FC<{
	className?: JSX.IntrinsicElements["div"]["className"];
}> = ({ children, className }) => (
	<div className={className}>
		{children}
	</div>
)
