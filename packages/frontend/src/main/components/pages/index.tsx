import React from "react"
import styled from "@emotion/styled"
import { PageBase } from "../stateless/common/page_base"
import { Views } from "../stateless/views"

const Red = styled(Views.Basic)`
	color: red;
`

const Spacer = styled(Views.Basic)`
	height: 5rem;
`

const Centered = styled(Views.Basic)`
	display: flex;
	justify-content: center;
	align-items: center;
`

const Image = styled.img`
	transition: filter 2s;
	&:hover {
		filter: saturate(50);
	}
`

export const MainPage = () => (
	<PageBase>
		<Centered>
			<Views.Basic>
				<Red>Hello world!</Red>
				<Spacer />
				<Image
					// eslint-disable-next-line max-len
					src="https://www.thesprucepets.com/thmb/xdBOcy1ctLYF7j3y1vaDtVijWxM=/2997x2248/smart/filters:no_upscale()/kitten-looking-at-camera-521981437-57d840213df78c583374be3b.jpg"
					width="300"
					alt="Some cute kitten for ya"
				/>
			</Views.Basic>
		</Centered>
	</PageBase>
)
