import "@emotion/react"
import ThemeX from "../../../theming/theme"

declare module "@emotion/react" {
	// `type` declaration can not merge with interfaces,
	// so it can not be used here.
	// eslint-disable-next-line @typescript-eslint/no-empty-interface
	export interface Theme extends ThemeX {}
}
